//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>

typedef struct{
    int number;
    int velocity;
    int channel;
} Note;

class MidiMessage
{
public:
    MidiMessage() //Constructor
    {
        note.number = 60;
        note.velocity = 127;
        note.channel = 1;
        std::cout << "Constructor\n";
    }
    MidiMessage (int initialNoteNumber, int initialVelocity, int initialChannel) //Constructor
    {
        note.number = initialNoteNumber;
        note.velocity = initialVelocity;
        note.channel = initialChannel;
    }
    ~MidiMessage() //Destructor
    {
        std::cout << "Destructor\n";
    }
    void setNoteNumber (int newNote) //Mutator
    {
        if(newNote >127)
        {
            newNote = 127;
        }
        else if(newNote <0)
        {
            newNote = 0;
        }
        note.number = newNote;
    }
    void setChannelNumber (int newChannel) //Mutator
    {
        if(newChannel >127)
        {
            newChannel = 127;
        }
        else if(newChannel <0)
        {
            newChannel = 0;
        }
        note.channel = newChannel;
    }
    void setVelocity (int newVelocity) //Mutator
    {
        if(newVelocity >127)
        {
            newVelocity = 127;
        }
        else if(newVelocity <0)
        {
            newVelocity = 0;
        }
        note.velocity = newVelocity;
    }
    int getNoteNumber() const //Accessor
    {
        return note.number;
    }
    int getChannelNumber() const //Accessor
    {
        return note.channel;
    }
    int getVelocity() const //Accessor
    {
        return note.velocity;
    }
    float getMidiNoteInHertz() const //Accessor
    {
        return 440 * pow(2, (note.number - 69) / 12.0);
    }
    float getFloatVelocity() const //Accessor
    {
        return note.velocity/127;
    }
    
private:
    Note note;
};

int main (int argc, const char* argv[])
{
    MidiMessage message(48, 60, 5);
        
    std::cout << "Note Number:" << message.getNoteNumber() << "\n";
    std::cout << "Note Frequency:" << message.getMidiNoteInHertz() << "\n";
    std::cout << "Note Channel:" << message.getChannelNumber() << "\n";
    std::cout << "Note Velocity:" << message.getVelocity() << "\n";
    return 0;
}

